(function() {
    'use strict';

	// https://bitbucket.org/morglod/mocomponents/src

    const version = 1;

    if(!(window && window.document)) {
        throw 'Export error';
    }

    // Check is already defined?
    if(window.MoComponentst && window.MoComponents.version == version) {
        return;
    }

    window.MoComponents = {
        version: version,
        components: {},
        define: function(args) {
            if(this.components[args.tag]);
            else {
                this._define(args);
            }
        },
        _define: function({tag, created, attached, attributeSet, detached, extend, style, baseElement, configPrototype, properties, template}) {
            let currentScript = document._currentScript || document.currentScript;

            if(tag.indexOf('-') == -1) {
                throw `Bad tag name: '${tag}', at least one '-' should be in name, eg 'my-tag'.`;
            }

            if(typeof(template) == 'string') {
                template = currentScript.ownerDocument.getElementById(template);
            }

            let prototype = Object.create(baseElement ? baseElement.prototype : HTMLElement.prototype, properties);
            prototype.createdCallback = function() {
                if(template) {
                    let templateContent = template.content;

                    let rootChildren = [];

                    if(templateContent.querySelector('component-content') != null) {
                        for(let i = 0; i < this.childNodes.length; ++i) {
                            rootChildren.push(this.childNodes[i]);
                        }
                        for(let i = 0; i < rootChildren.length; ++i) {
                            rootChildren[i].remove();
                        }
                    }

                    let clone = document.importNode(templateContent, true);
                    this.appendChild(clone);

                    if(style) {
                        let value = this.getAttribute('style');
                        this.setAttribute('style', '/* component style: */ ' + style + (value ? '; /**/ ' + value : '; /**/'));
                    }

                    let classAttrib = template.getAttribute('class');
                    if(classAttrib) this.setAttribute('class', classAttrib);

                    // Process <content select='tag name'>
                    let contentSelectNodes = this.querySelectorAll('component-content[selectTag]');

                    for(let i = 0; i < contentSelectNodes.length; ++i) {
                        let contentNode = contentSelectNodes[i];
                        let attr = contentNode.getAttribute('selectTag');
                        let tagName = attr.toUpperCase();
                        let contentNodeParent = contentNode.parentNode;
                        for(let j = 0; j < rootChildren.length; ++j) {
                            if(rootChildren[j] != null && rootChildren[j].tagName == tagName) {
                                contentNodeParent.insertBefore(rootChildren[j], contentNode);
                                rootChildren[j] = null;
                            }
                        }
                        contentNode.remove();
                    }

                    let contentNodes = this.querySelectorAll('component-content:not([selectTag])');
                    for(let i = 0; i < contentNodes.length; ++i) {
                        let contentNode = contentNodes[i];
                        let contentNodeParent = contentNode.parentNode;
                        for(let j = 0; j < rootChildren.length; ++j) {
                            if(rootChildren[j] != null) {
                                contentNodeParent.insertBefore(rootChildren[j], contentNode);
                                rootChildren[j] = null;
                            }
                        }
                        contentNode.remove();
                    }
                }

                if(created) created.bind(this)();
            };
            prototype.attachedCallback = function(abc) {
                if(attached) attached.bind(this)();
                if(attributeSet) {
                    let attributeSetThis = attributeSet.bind(this);
                    for(let i = 0; i < this.attributes.length; ++i) {
                        attributeSetThis({
                            name: this.attributes[i].name,
                            old: null,
                            value: this.attributes[i].value
                        });
                    }
                }
                if(this.onload) {
                    this.onload();
                }
            };
            prototype.detachedCallback = function() {
                if(detached) detached.bind(this)();
            };
            prototype.attributeChangedCallback = function(name, old, value) {
                if(attributeSet) attributeSet.bind(this)({name, old, value});
            };
            if(configPrototype) configPrototype(prototype);

            try {
                document.registerElement(tag, {
                    extends: extend,
                    prototype: prototype
                });

                this.components[tag] = {
                    template: template,
                    created: created,
                    attached: attached,
                    detached: detached,
                    configPrototype: configPrototype,
                    baseElement: baseElement,
                    properties: properties // https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperties
                };
            } catch(e) {}
        }
    };

    window.MoComponents.define({
        tag: 'component-content'
    });
})();

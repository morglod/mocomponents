# MoComponents #

Simple workaround on CustomComponents.

Similar to Polymer, but without some bugs with 'content' tag and super small.

### HOWTO ###

[How custom components work?](http://webcomponents.org/articles/introduction-to-custom-elements/)

**Be sure to enable 'WebComponents' support in your browser.**

First include library to your web page:
```
#!html

<script src='./components.min.js'></script>
```

Define your custom tag:
```
#!html

<template id='template1'>
    <h1>Hello world!</h1>
    <component-content></component-content>
    <text>It works!</text>
</template>

<script>
MoComponents.define({
	tag: 'hello-world',
	template: document.getElementById('template1'),
	created: function() {
		console.log("Hello world!")
	}
});
</script>
```

and somewhere in page:
```
#!html

<hello-world style='background-color: red'>
    <text>Wow</text>
</hello-world>
```

Result will be:

![QIP Shot - Screen 847.png](https://bitbucket.org/repo/LBpzXr/images/3340129067-QIP%20Shot%20-%20Screen%20847.png)

You may define inline style for all elements of this tag:

```
#!javascript

MoComponents.define({
	tag: 'hello-world',
	template: document.getElementById('template1'),
	style: 'display: block',
	created: function() {
		console.log("Hello world!")
	}
});
```

Result:

![QIP Shot - Screen 848.png](https://bitbucket.org/repo/LBpzXr/images/1522225246-QIP%20Shot%20-%20Screen%20848.png)

### If you use it ###

[Send me a private message about your project](https://bitbucket.org/account/notifications/send/?receiver=morglod)